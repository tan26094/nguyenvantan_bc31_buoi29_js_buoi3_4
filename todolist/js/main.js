// ----------------------------Function lấy danh sách tag----------------------------
let getTaskList = () => {};
window.getTaskList = getTaskList;
// ----------------------------Function render task single task----------------------------
let renderTask = (taskList) => {
  let taskRemain_HTML = "";
  let taskCompleted_HTML = "";

  for (let i = 0; i < taskList.length; i++) {
    let task = taskList[i];
    console.log(task);
    if (task.status) {
      taskCompleted_HTML += `<li>
                            <span class=".fas">${task.id}. ${task.task}</span> 
                            <div class="buttons">
                                <i class="fa fa-trash-alt remove" onclick="deleteTask(${task.id})"></i>
                                <span><i class="fa fa-check-circle fas" onclick="taskCompleted(${task.id})"></i></span> 
                            </div>
                            </li>`;
    } else {
      taskRemain_HTML += `<li>
                                    ${task.id}. ${task.task}
                                    <div class="buttons">
                                        <i class="fa fa-trash-alt remove" onclick="deleteTask(${task.id})"></i>
                                        <i class="fa fa-check-circle complete" onclick="taskCompleted(${task.id})"></i>
                                    </div>
                                </li>`;
    }
    document.getElementById("todo").innerHTML = taskRemain_HTML;
    document.getElementById("completed").innerHTML = taskCompleted_HTML;
  }
};
window.renderTask = renderTask;
// ----------------------------Function render task list----------------------------
let renderTaskList = () => {
  console.log("In render task");
  //   Get task list
  taskService
    .getTaskList()
    .then((res) => {
      console.log(res.data);
      renderTask(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
  //
};
window.renderTaskList = renderTaskList;
// ----------------------------Function delete task----------------------------
let deleteTask = (idTask) => {
  console.log("In delete task", idTask);
  taskService
    .deleteTask(idTask)
    .then((res) => {
      console.log("task deleted", res);
      renderTaskList();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.deleteTask = deleteTask;
// ----------------------------Function add task----------------------------
let addTask = () => {
  let taskName = taskController.layThongTinTuForm();
  console.log("In delete task", taskName);
  taskService
    .addTask(taskName)
    .then((res) => {
      console.log("task added", res);
      renderTaskList();
      taskController.cleanForm();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.addTask = addTask;
import { taskController } from "./controller/taskController.js";
// ----------------------------Function add task----------------------------
let taskCompleted = (idTask) => {
  let taskDetail = null;
  taskService
    .getTaskDetail(idTask)
    .then((res) => {
      taskDetail = res.data;
      console.log("taskDetail: ", taskDetail.status);
      if (taskDetail.status) {
        taskDetail.status = false;
      } else {
        taskDetail.status = true;
      }
      let newTask = {
        status: taskDetail.status,
      };
      console.log("taskDetail: ", taskDetail.status);
      taskService
        .taskCompleted(idTask, newTask)
        .then((res) => {
          console.log(res);
          renderTaskList();
        })
        .catch((err) => {
          console.log(err);
        });
    })
    .catch((err) => {
      console.log(err);
    });
};
window.taskCompleted = taskCompleted;
// ----------------------------Excuse----------------------------
import { taskService } from "./service/taskService.js";

// Chạy lần đầu khi load lại trang

renderTaskList();
