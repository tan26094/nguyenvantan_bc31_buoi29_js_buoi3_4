const BaseURL = "https://62c2fe8eff594c65676b786c.mockapi.io/todo/";
export let taskService = {
  getTaskList: () => {
    return axios({
      url: BaseURL,
      method: "GET",
    });
  },
  getTaskDetail: (idTask) => {
    return axios({
      url: `${BaseURL}${idTask}`,
      method: "GET",
    });
  },
  addTask: (taskname) => {
    return axios({
      url: BaseURL,
      method: "POST",
      data: taskname,
    });
  },
  deleteTask: (idTask) => {
    return axios({
      url: `${BaseURL}${idTask}`,
      method: "DELETE",
    });
  },
  taskCompleted: (idTask, newStatus) => {
    return axios({
      url: `${BaseURL}${idTask}`,
      method: "PUT",
      data: newStatus,
    });
  },
};
