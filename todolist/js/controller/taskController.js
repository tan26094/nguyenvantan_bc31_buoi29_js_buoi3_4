export let taskController = {
  layThongTinTuForm: () => {
    let newTask = document.getElementById("newTask").value;
    let task = {
      task: newTask,
    };
    return task;
  },
  cleanForm: () => {
    document.getElementById("newTask").value = "";
  },
};
